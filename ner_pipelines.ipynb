{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "c5151671",
   "metadata": {},
   "source": [
    "## Processing Pipelines for Named Entity Recognition with flair, spacy and stanza"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "feab68e3",
   "metadata": {},
   "source": [
    "The goal of this project was to compare and validate the results of the named entity recognition methods of three popular NLP packages in Python, `flair`, `spacy` and `stanza`. For the validation, a dataset consisting of 887 news articles from two German media titles, *Die Welt* and *Der Spiegel*, and one German news agency, *DPA*, was used. These articles were compiled as part of a manual content analysis that explored news coverage on Covid-19 in German media from January to June 2020.\n",
    "\n",
    "In this analysis, all actors that were cited directly or indirectly in an article were recorded manually. This list of actors was later used to calculate the recall of the different methods (the share of actors present in the articles that was correctly identified). The precision (the share of identified actors that are actually actors) was calculated by manually checking all identified entities.\n",
    "\n",
    "An important part of the project was to transform the documents that were used in the manual analysis into a format that NLP methods can work with. The articles were initially exported as PDF files from the LexisNexis and DPA databases. These PDFs were then manually exported as text files using a PDF reader program. The resulting text files still contained some metadata such as copyright information or the source title and thus had to be preprocessed before they could be used for the analysis. After preprocessing, we used the functions implemented in the three packages to identify named entities and extracted the different sorts of entities (actors, organisations, locations and miscellaneous)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "863bd8e1",
   "metadata": {},
   "source": [
    "### Packages and dataset\n",
    "\n",
    "In addition to the three NLP packages, we used the `re` and `pandas` packages. re is included in the [Python standard library](https://docs.python.org/3/library/) and can be used for regular expression operations. [pandas](https://pandas.pydata.org/) is a widely used package that contains a number of useful classes and functions for data transformation and analysis. Throughout our analysis, we stored all data in pandas dataframes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "c2515bdb",
   "metadata": {},
   "outputs": [],
   "source": [
    "import stanza, flair, spacy\n",
    "import re\n",
    "import pandas as pd"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a359358a",
   "metadata": {},
   "source": [
    "All documents were saved in one text file that still contained metadata such as the source of the article, the publication date or copyright information. We used this metadata to split the text file into single articles. *DPA* articles end with \"Copyright: dpa\", articles from *Die Welt* and *Der Spiegel* end with \"End of Document\". After splitting and unpacking the single articles, the last row is dropped as it contains an empty document."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "c3912718",
   "metadata": {},
   "outputs": [],
   "source": [
    "with open(\"data/corona_all_articles_887_new.txt\", \"r\", encoding = \"utf-8\") as file:\n",
    "    docs = [doc.split('Copyright: dpa') for doc in file.read().split('End of Document')]\n",
    "docs = [doc for doc_list in docs for doc in doc_list]\n",
    "all_articles = pd.DataFrame(docs, columns = [\"content\"])\n",
    "all_articles.drop(all_articles.tail(1).index, inplace = True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "29f9d09d",
   "metadata": {},
   "source": [
    "### Preprocessing\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d7c80f84",
   "metadata": {},
   "source": [
    "Before we could extract named entities with the different packages, the documents had to be cleaned as they still contained metadata that could possibly lead to mistakes during the extraction or classification of named entities. First, we removed some special characters and multiple spaces or newlines."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "a8fea926",
   "metadata": {},
   "outputs": [],
   "source": [
    "all_articles[\"content_clean\"] = all_articles.content.apply(lambda x: re.sub(\"\\xa0\", \" \", re.sub(\"\\ufeff\", \" \", x)).strip())\n",
    "all_articles[\"content_clean\"] = all_articles.content_clean.apply(lambda x: re.sub(\"\\s{2,}\", \" \", re.sub(\"\\n{2,}\", \"\\n\", x)))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e4bf542f",
   "metadata": {},
   "source": [
    "Next, we classified all articles based on their source. Articles in *Die Welt* and *Der Spiegel* contain a source reference at the beginning of each article. This information is required later on because the structure of the documents differs between the three media titles."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "d076bc01",
   "metadata": {},
   "outputs": [],
   "source": [
    "all_articles[\"source\"] = \"none\"\n",
    "for i, item in enumerate(all_articles.content_clean):\n",
    "    if re.search(r\"Die Welt[ \\n][A-Z][a-z]* \\d+.\", item):\n",
    "        all_articles.loc[i, \"source\"] = \"Die Welt\"\n",
    "    elif re.search(r\"Der Spiegel\\n\\d+.\", item):\n",
    "        all_articles.loc[i, \"source\"] = \"Der Spiegel\"\n",
    "    else:\n",
    "        all_articles.loc[i, \"source\"] = \"dpa\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "db6da255",
   "metadata": {},
   "source": [
    "The vast majority of the articles in our analysis was published by the *DPA*."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "1296279a",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "dpa            710\n",
       "Die Welt       140\n",
       "Der Spiegel     37\n",
       "Name: source, dtype: int64"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "all_articles.source = all_articles.source.astype(\"category\")\n",
    "all_articles.source.value_counts()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "59e708de",
   "metadata": {},
   "source": [
    "To extract the title and document body, we used the structure of the documents. In *Welt* and *Spiegel*, the documents start with the article title, followed by the source reference and metadata such as the publication date, section or length. After that, the actual article starts after \"Body\" and ends with \"Load-Date\" or in some cases with \"Graphic\" (see screenshot of example document below).\n",
    "\n",
    "#### Example document 1     \n",
    "![Example document from Die Welt](img/example_document_welt.png \"Example document from Die Welt\")\n",
    "\n",
    "The structure of *DPA* articles is a bit more complicated. Documents start with a varying number of lines containing abbreviations and numbers that stand for metadata such as the channels used to distribute the article or the section. Often, but not always, the publication date is presented before these lines. The next line contains a number of keywords and is followed by the title and body of the article. The body ends with \"Notizblock\" and additional metadata (see second screenshot of example document). Because the structure of the initial metadata is not really uniform, we had to use multiple regular expressions to extract the title.\n",
    "\n",
    "#### Example document 2    \n",
    "![Example document from DPA](img/example_document_dpa.png \"Example document from DPA\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "16679001",
   "metadata": {},
   "outputs": [],
   "source": [
    "all_articles[\"title\"] = \"none\"\n",
    "for i, item in enumerate(all_articles.content_clean):\n",
    "    if all_articles.loc[i, \"source\"] == \"Der Spiegel\":\n",
    "        all_articles.loc[i, \"title\"] = item[:(re.search(r\"Der Spiegel\\n\\d+.\", item).start()-1)]\n",
    "    elif all_articles.loc[i, \"source\"] == \"Die Welt\":\n",
    "        all_articles.loc[i, \"title\"] = item[:(re.search(r\"Die Welt[ \\n][A-Z][a-z]* \\d+.\", item).start()-1)]\n",
    "    else:\n",
    "        try:\n",
    "            temp = item[(re.search(r\"(([\\w\\-]+ ){5,6}[\\d]+\\n)+([A-Z\\dÖÜÄ#][\\w\\.\\-äöüß#]* )+[\\wäöüßÖÜÄ\\.\\-#]+\\n\", item).end()):]\n",
    "            all_articles.loc[i, \"title\"] = temp[:(re.search(r\"\\n\", temp).start())]\n",
    "        except(AttributeError):\n",
    "            continue"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "id": "5b54e259",
   "metadata": {},
   "outputs": [],
   "source": [
    "for i, item in enumerate(all_articles.content_clean):\n",
    "    if all_articles.loc[i, \"title\"] == \"none\":\n",
    "        try:\n",
    "            temp = item[(re.search(r\"([A-Z\\d#][äöüß\\w\\-#]+ )+[A-Z\\d#][äöüß\\w\\-#]+\\n\", item).end()):]\n",
    "            all_articles.loc[i, \"title\"] = temp[:(re.search(r\"\\n\", temp).start())]\n",
    "        except(AttributeError):\n",
    "            continue       "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "id": "d9fe3627",
   "metadata": {},
   "outputs": [],
   "source": [
    "for i, item in enumerate(all_articles.content_clean):\n",
    "    if all_articles.loc[i, \"title\"] == \"none\":\n",
    "        try:\n",
    "            temp = item[(re.search(r\"(([\\w\\-]+ ){5,6}[\\d]+\\n)+([A-Za-z\\dÖÜÄ#][\\w\\.\\-äöüß#]* )+[\\wäöüßÖÜÄ\\.\\-#]+\\n\", item).end()):]\n",
    "            all_articles.loc[i, \"title\"] = temp[:(re.search(r\"\\n\", temp).start())]\n",
    "        except(AttributeError):\n",
    "            continue     "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "id": "45c644db",
   "metadata": {},
   "outputs": [],
   "source": [
    "all_articles[\"title\"] = all_articles[\"title\"].str.strip()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "id": "3d57f19e",
   "metadata": {},
   "outputs": [],
   "source": [
    "all_articles[\"body\"]=\"none\"\n",
    "for i, item in enumerate(all_articles.content_clean):\n",
    "    if (all_articles.loc[i, \"source\"] == \"Der Spiegel\") | (all_articles.loc[i, \"source\"] == \"Die Welt\"):\n",
    "        if 'Graphic\\n' in item:\n",
    "            all_articles.loc[i, \"body\"] = item[(item.find('\\nBody') + 5):item.find('Graphic\\n')].strip()\n",
    "        else:\n",
    "            all_articles.loc[i, \"body\"] = item[(item.find('\\nBody') + 5):item.find('\\nLoad-Date')].strip()\n",
    "    else:\n",
    "        all_articles.loc[i, \"body\"] = item[(item.find(all_articles.loc[i, \"title\"])+len(all_articles.loc[i, \"title\"])):item.find(\"\\nNotizblock\")].strip()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "17445200",
   "metadata": {},
   "source": [
    "After extracting the title and body, we removed source references in the *DPA* articles and multiple spaces."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "id": "511ec9d0",
   "metadata": {},
   "outputs": [],
   "source": [
    "all_articles[\"body\"] = all_articles[\"body\"].apply(lambda x: re.sub(r\"\\(dpa\\)\", \" \", x))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "id": "deb3ad28",
   "metadata": {},
   "outputs": [],
   "source": [
    "all_articles[\"body\"] = all_articles[\"body\"].apply(lambda x: re.sub(r\"\\(dpa/lsw\\)\", \" \", x))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "id": "35987d30",
   "metadata": {},
   "outputs": [],
   "source": [
    "all_articles[\"body\"] = all_articles[\"body\"].apply(lambda x: re.sub(r\"\\s+\", \" \", x))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aebcb4d9",
   "metadata": {},
   "source": [
    "We did not have to perform additional preprocessing steps such as tokenizing, stop word removal or stemming that are used as part of other NLP methods. `SpaCy` and `Stanza` perform tokenizing as part of the pipeline used to extract named entities (`flair` contains a method for tokenizing which we used before named entity recognition with this package) and the other preprocessing steps are not necessary for the identification of named entities."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2e8c2207",
   "metadata": {},
   "source": [
    "### spaCy"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bae4d5fc",
   "metadata": {},
   "source": [
    "We used the [de_core_news_lg model](https://spacy.io/models/de#de_core_news_lg) in spaCy as it had the highest accuracy of the German models implemented in spaCy. The model was trained on the WIkiNER (Wikipedia articles) and Tiger Corpus (articles from the German newspaper *Frankfurter Rundschau*)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "id": "01e2a824",
   "metadata": {},
   "outputs": [],
   "source": [
    "import de_core_news_lg\n",
    "nlp_spacy = de_core_news_lg.load()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "id": "41262bc1",
   "metadata": {},
   "outputs": [],
   "source": [
    "all_articles[\"spacy_doc\"] = all_articles.body.apply(nlp_spacy)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "fb7a4573",
   "metadata": {},
   "outputs": [],
   "source": [
    "all_articles[\"spacy_entities\"] = all_articles.spacy_doc.apply(lambda x: x.ents)\n",
    "all_articles[\"spacy_persons\"] = all_articles.spacy_entities.apply(lambda x: [entity for entity in x if (entity.label_ == \"PERSON\") or (entity.label_ == \"PER\")])\n",
    "all_articles[\"spacy_organisations\"] = all_articles.spacy_entities.apply(lambda x: [entity for entity in x if entity.label_ == \"ORG\"])\n",
    "all_articles[\"spacy_locations\"] = all_articles.spacy_entities.apply(lambda x: [entity for entity in x if entity.label_ == \"LOC\"])\n",
    "all_articles[\"spacy_misc\"] = all_articles.spacy_entities.apply(lambda x: [entity for entity in x if entity.label_ == \"MISC\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a7b3e886",
   "metadata": {},
   "source": [
    "### Stanza"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6085a114",
   "metadata": {},
   "source": [
    "For NER with Stanza, we used the model trained on the [GermEval2014 corpus](https://sites.google.com/site/germeval2014ner/data) as it was the German model with the highest accuracy according to Stanza. The corpus contains Wikipedia sites and articles from online newspapers. The default tokenizer was used."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "id": "d410cb40",
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Downloading https://raw.githubusercontent.com/stanfordnlp/stanza-resources/master/resources_1.2.0.json: 128kB [00:00, 42.9MB/s]\n",
      "2021-05-03 09:52:23 WARNING: Language de package default expects mwt, which has been added\n",
      "2021-05-03 09:52:23 INFO: Downloading these customized packages for language: de (German)...\n",
      "==================================\n",
      "| Processor       | Package      |\n",
      "----------------------------------\n",
      "| tokenize        | gsd          |\n",
      "| mwt             | gsd          |\n",
      "| ner             | germeval2014 |\n",
      "| forward_charlm  | newswiki     |\n",
      "| backward_charlm | newswiki     |\n",
      "==================================\n",
      "\n",
      "Downloading http://nlp.stanford.edu/software/stanza/1.2.0/de/tokenize/gsd.pt: 100%|██| 652k/652k [00:00<00:00, 699kB/s]\n",
      "Downloading http://nlp.stanford.edu/software/stanza/1.2.0/de/mwt/gsd.pt: 100%|███████| 546k/546k [00:01<00:00, 386kB/s]\n",
      "Downloading http://nlp.stanford.edu/software/stanza/1.2.0/de/ner/germeval2014.pt: 100%|█| 351M/351M [02:15<00:00, 2.58M\n",
      "2021-05-03 09:54:49 INFO: File exists: C:\\Users\\NP\\stanza_resources\\de\\forward_charlm\\newswiki.pt.\n",
      "2021-05-03 09:54:49 INFO: File exists: C:\\Users\\NP\\stanza_resources\\de\\backward_charlm\\newswiki.pt.\n",
      "2021-05-03 09:54:49 INFO: Finished downloading models and saved to C:\\Users\\NP\\stanza_resources.\n",
      "2021-05-03 09:54:49 WARNING: Language de package default expects mwt, which has been added\n",
      "2021-05-03 09:54:49 INFO: Loading these models for language: de (German):\n",
      "============================\n",
      "| Processor | Package      |\n",
      "----------------------------\n",
      "| tokenize  | gsd          |\n",
      "| mwt       | gsd          |\n",
      "| ner       | germeval2014 |\n",
      "============================\n",
      "\n",
      "2021-05-03 09:54:49 INFO: Use device: cpu\n",
      "2021-05-03 09:54:49 INFO: Loading: tokenize\n",
      "2021-05-03 09:54:49 INFO: Loading: mwt\n",
      "2021-05-03 09:54:49 INFO: Loading: ner\n",
      "2021-05-03 09:54:50 INFO: Done loading processors!\n"
     ]
    }
   ],
   "source": [
    "processor_dict = {\n",
    "    'tokenize': 'default', \n",
    "    'ner': 'germeval2014',\n",
    "}\n",
    "stanza.download('de', processors=processor_dict, package=None)\n",
    "nlp_stanza = stanza.Pipeline('de', processors=processor_dict, package=None)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 35,
   "id": "7a8b6929",
   "metadata": {},
   "outputs": [],
   "source": [
    "all_articles[\"stanza_doc\"] = all_articles.body.apply(nlp_stanza)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "f72f8510",
   "metadata": {},
   "outputs": [],
   "source": [
    "all_articles[\"stanza_persons\"] = all_articles.stanza_doc.apply(lambda x: [ent.text for sent in x.sentences for ent in sent.ents if (ent.type == \"PERSON\") or (ent.type == \"PER\")])\n",
    "all_articles[\"stanza_organisations\"] = all_articles.stanza_doc.apply(lambda x: [ent.text for sent in x.sentences for ent in sent.ents if (ent.type == \"ORGANISATION\") or (ent.type == \"ORG\")])\n",
    "all_articles[\"stanza_locations\"] = all_articles.stanza_doc.apply(lambda x: [ent.text for sent in x.sentences for ent in sent.ents if (ent.type == \"LOCATION\") or (ent.type == \"LOC\")])\n",
    "all_articles[\"stanza_misc\"] = all_articles.stanza_doc.apply(lambda x: [ent.text for sent in x.sentences for ent in sent.ents if (ent.type == \"MISCELLANEOUS\") or (ent.type == \"MISC\")])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f6920ad6",
   "metadata": {},
   "source": [
    "### Flair"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ffdada4a",
   "metadata": {},
   "source": [
    "For the extraction of named entities with flair, we used the [default model](https://huggingface.co/flair/ner-german) that was trained on the CoNLL-03 German corpus. This corpus contains articles from the German newspaper *Frankfurter Rundschau*. Tokenization was performed with the segtok sentence splitter."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "93285d1b",
   "metadata": {},
   "outputs": [],
   "source": [
    "splitter = flair.tokenization.SegtokSentenceSplitter()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "eb4e866a",
   "metadata": {},
   "outputs": [],
   "source": [
    "all_articles[\"tokenized_sentences_flair\"] = all_articles.body.apply(lambda x: splitter.split(x))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "5068d41a",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2021-05-03 11:10:23,638 --------------------------------------------------------------------------------\n",
      "2021-05-03 11:10:23,642 The model key 'de-ner' now maps to 'https://huggingface.co/flair/ner-german' on the HuggingFace ModelHub\n",
      "2021-05-03 11:10:23,643  - The most current version of the model is automatically downloaded from there.\n",
      "2021-05-03 11:10:23,644  - (you can alternatively manually download the original model at https://nlp.informatik.hu-berlin.de/resources/models/de-ner/de-ner-conll03-v0.4.pt)\n",
      "2021-05-03 11:10:23,644 --------------------------------------------------------------------------------\n"
     ]
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "1a7e2919b98744cf9c8aff81b22d9e6e",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "Downloading:   0%|          | 0.00/1.51G [00:00<?, ?B/s]"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2021-05-03 11:15:36,246 loading file C:\\Users\\NP\\.flair\\models\\ner-german\\a125be40445295f7e94d0afdb742cc9ac40ec4e93259dc30f35220ffad9bf1f6.f46c4c5cfa5e34baa838983373e30051cd1cf1e933499408a49e451e784b0a11\n"
     ]
    }
   ],
   "source": [
    "tagger = flair.models.SequenceTagger.load(\"de-ner\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "7d6ab7c1",
   "metadata": {},
   "outputs": [],
   "source": [
    "for item in all_articles.tokenized_sentences_flair:\n",
    "    tagger.predict(item)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "81f5b78f",
   "metadata": {},
   "outputs": [],
   "source": [
    "all_articles[\"flair_ner_dicts\"] = all_articles.tokenized_sentences_flair.apply(lambda x: [sentence.to_dict(tag_type='ner') for sentence in x])\n",
    "all_articles[\"flair_persons\"] = all_articles.flair_ner_dicts.apply(lambda x: [ent[\"text\"] for sentence in x for ent in sentence[\"entities\"] if ent[\"labels\"][0].value == \"PER\"])\n",
    "all_articles[\"flair_organisations\"] = all_articles.flair_ner_dicts.apply(lambda x: [ent[\"text\"] for sentence in x for ent in sentence[\"entities\"] if ent[\"labels\"][0].value == \"ORG\"])\n",
    "all_articles[\"flair_locations\"] = all_articles.flair_ner_dicts.apply(lambda x: [ent[\"text\"] for sentence in x for ent in sentence[\"entities\"] if ent[\"labels\"][0].value == \"LOC\"])\n",
    "all_articles[\"flair_misc\"] = all_articles.flair_ner_dicts.apply(lambda x: [ent[\"text\"] for sentence in x for ent in sentence[\"entities\"] if ent[\"labels\"][0].value == \"MISC\"])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "1b7e5976",
   "metadata": {},
   "outputs": [],
   "source": [
    "for colname in ['spacy_persons', 'spacy_organisations', 'spacy_locations', 'spacy_misc', 'stanza_persons', 'stanza_organisations', 'stanza_locations', 'stanza_misc', 'flair_persons', 'flair_organisations', 'flair_locations', 'flair_misc']:\n",
    "    all_articles[colname] = all_articles[colname].apply(lambda x: \"; \".join(x))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "75498681",
   "metadata": {},
   "outputs": [],
   "source": [
    "all_articles.loc[:,['source', 'title', 'spacy_persons', 'spacy_organisations', 'spacy_locations', 'spacy_misc', 'stanza_persons', 'stanza_organisations', 'stanza_locations', 'stanza_misc', 'flair_persons', 'flair_organisations', 'flair_locations', 'flair_misc']].to_csv(\"data/all_packages_all_entities.csv\", index = False, sep = \",\", encoding = \"UTF8\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
